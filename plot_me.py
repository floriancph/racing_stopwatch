from tkinter import * 
from tkinter.ttk import *
from tkinter import ttk

from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
# from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
from tkinter.filedialog import asksaveasfile 

import helper, save_me

def plotting(root, lap_times_delta, driver_name, times_text_object_variable, start_times):
    plot_window = Toplevel(root)
    plot_window.configure(background='white')
    
    x = list(range(1, len(lap_times_delta)+1))
    y = []
    for delta_time in lap_times_delta:
        p = helper.min_to_sec(lap_times_delta[delta_time])
        y.append(p)

    head_plot = Label(plot_window, font = ('calibri', 20, 'bold'), 
        background = 'white', 
        foreground = 'black')
    head_plot.pack(anchor = 'ne')
    head_plot.config(text = f"Analysis ({driver_name})")

    fig = Figure(figsize=(5, 4), dpi=100)
    times_plot = fig.add_subplot(211)
    times_plot.set_title("Laptime Distribution - " + str(driver_name))
    times_plot.set_ylabel('Laptimes (in Sec.)')
    times_plot.plot(x, y)

    box_plot = fig.add_subplot(212)
    box_plot.set_ylabel('Laptimes (in Sec.)')
    box_plot.boxplot(y)

    canvas = FigureCanvasTkAgg(fig, master=plot_window)
    canvas.draw()
    canvas.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)

    toolbar = NavigationToolbar2Tk(canvas, plot_window)
    toolbar.update()
    canvas.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)

    font_size = 14

    lbl_min = Label(plot_window, font = ('calibri', font_size, 'bold'))
    lbl_min.configure(background='white')
    lbl_min.config(text = "Best Lap: " + str(round(min(y), 3)) + " Sec.")
    lbl_min.pack()
    
    lbl_median = Label(plot_window, font = ('calibri', font_size, 'bold'))
    lbl_median.configure(background='white')
    lbl_median.config(text = "Median: " + str(round(helper.get_median(y), 3)) + " Sec.")
    lbl_median.pack()

    lbl_mean = Label(plot_window, font = ('calibri', font_size, 'bold'))
    lbl_mean.configure(background='white')
    lbl_mean.config(text = "Mean: " + str(round(helper.get_mean(y), 3)) + " Sec.")
    lbl_mean.pack()
    
    lbl_sd = Label(plot_window, font = ('calibri', font_size, 'bold'))
    lbl_sd.configure(background='white')
    lbl_sd.config(text = "Standard Deviation: " + str(round(helper.get_sd(y), 3)) + " Sec.")
    lbl_sd.pack()
    
    lbl_max = Label(plot_window, font = ('calibri', font_size, 'bold'))
    lbl_max.configure(background='white')
    lbl_max.config(text = "Slowest Lap: " + str(round(max(y), 3)) + " Sec.")
    lbl_max.pack()

    btn_save_summary = ttk.Button(plot_window, text = 'Save Summary', command = lambda : save_me.save_summary(start_times, lap_times_delta, times_text_object_variable))
    btn_save_summary.place(x=0, y=0, width = 130)
    btn_save_csv = ttk.Button(plot_window, text = 'Save CSV', command = lambda : save_me.save_csv(lap_times_delta))
    btn_save_csv.place(x=0, y=30, width = 130)