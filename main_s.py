from tkinter import * 
from tkinter import messagebox
from tkinter.ttk import *
from tkinter import ttk
from os import path

from PIL import ImageTk, Image
# Install Pillow to make it work

import driver, plot_me, local_config, helper

def add_driver(event=None):
    """Start a new Stopwatch for a new Driver"""
    global active_drivers
    global caching
    d = driver.Driver(root, s.get(), local_config.team_name, caching)
    active_drivers.append(d)
    current = a.cget("text")
    new = current + str(d.driver_name).strip() + "\n"
    a.config(text = new)
    btn_show_results.config(state="normal")

def show_results():
    """Show the Results of all active Drivers"""
    global active_drivers
    no_results = True
    for driver in active_drivers:
        if len(driver.lap_times_delta) > 0:
            no_results = False
            plot_me.plotting(root, driver.lap_times_delta, driver.driver_name, driver.times_text_object_variable, driver.start_times)
    if no_results:
        show_info_box('No Results up until now!')

def deleteText(event):
    """Deletes Text in TextField DriverName"""
    s.delete(0, "end")
    return None

def show_info_box(text):
    messagebox.showinfo(title='Info', message=text)

def _quit():
    """Quits Session"""
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        root.quit()     # stops mainloop
        root.destroy() # necessary for Windows

##### GUI #####
root = Tk() 
root.title(f'{local_config.team_name} Stopwatch')
root.configure(background='white')

active_drivers = []

try:
    # Image
    image_path = local_config.logo_path
    i = Image.open(image_path)
    # Resize Image: i = i.resize((250, 250), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(i)
    panel = Label(root, image = img)
    panel.configure(background='white')
    panel.pack(side = "top", fill = "both", expand = "yes", pady=5)
except:
    error_text = Label(root, foreground='red')
    error_text.config(text = f"place your image\n'{local_config.logo_name}' in\n{local_config.path_to_logo}")
    error_text.configure(background='white')
    error_text.pack(pady=5)

# Driver Name TextField
s = Entry(root, font=("Calibri 20"), width=15, justify='center')
s.configure(background='white')
s.pack()
s.insert(10,"[Driver Name]")
s.bind("<Button-1>", deleteText)

# Add Driver
btn_add_driver = Button(root, width=20, text="Create Driver [ENTER]", command=add_driver)
btn_add_driver.pack(pady=5)
btn_add_driver.focus_set()

# Active Drivers
a = Label(root, width=20)
a.configure(background='white')
a.config(text = "Active Drivers:\n")
a.pack(pady=5)

# Show All Results
btn_show_results = Button(root, width=20, text="Show Analysis", state=DISABLED, command=show_results)
btn_show_results.pack(pady=5)

# Quit Button
btn_quit = Button(master=root, text="Quit", command=_quit)
btn_quit.pack(side=BOTTOM, pady=10)

caching = False

if path.isdir(local_config.caching_path):
    caching_lbl = Label(master=root, foreground="green", text="Caching is activated")
    caching = True
else:
    caching_lbl = Label(master=root, foreground="red", text="Caching is deactivated")
    
caching_lbl.configure(background='white')
caching_lbl.pack(side=BOTTOM, pady=10)

# OnEnter add new Driver
root.bind('<Return>', add_driver)
root.protocol("WM_DELETE_WINDOW", _quit)

mainloop()