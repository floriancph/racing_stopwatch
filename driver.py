from tkinter import * 
from tkinter.ttk import *
from tkinter import messagebox
from tkinter import ttk
from tkinter import IntVar

from datetime import datetime, timedelta
from time import strftime 

import plot_me, save_me, helper

class Driver():

    def __init__(self, root, driver_name, team_name, caching):
        self.driver_name = driver_name.strip()
        self.driver_window = Toplevel(root)
        self.driver_window.configure(background='white')
        self.driver_window.title(f'Stopwatch for {str(self.driver_name)} [{team_name}]')

        # CLOCK
        self.header = Label(self.driver_window, font = ('calibri', 20, 'bold'), 
                background = 'white', 
                foreground = 'black')
        self.header.pack(anchor = 'center')
        self.header.config(text = "Driver: " + str(self.driver_name))
        
        self.lbl = Label(self.driver_window, font = ('calibri', 15, 'bold'), 
                background = 'white', 
                foreground = 'black')
        self.lbl.pack(anchor = 'center', pady=5)
        self.lbl.config(text = "00:00:00")

        # CURRENT COUNTDOWN TIME
        self.header2 = Label(self.driver_window, font = ('calibri', 30, 'bold'), 
                background = 'white', 
                foreground = 'black')
        self.header2.pack(anchor = 'center')
        self.header2.config(text = "Current Lap:")
        self.lbl_delta = Label(self.driver_window, font = ('calibri', 30, 'bold'), 
                background = 'white', 
                foreground = 'black')
        self.lbl_delta.pack(anchor = 'center')
        self.lbl_delta.config(text = "00:00:00.0000")

        # BUTTONS
        # Start Button
        self.btn_start = Button(self.driver_window, width=20, text="Start [TAB]", command=self.startBtn_OnClick)
        self.btn_start.pack(pady=5)
        self.driver_window.bind("<Tab>", self.startBtn_OnClick)
        # Lap Button
        self.btn_lap = Button(self.driver_window, width=20, text="Lap +1 [ENTER]", command=self.lap)
        self.btn_lap.pack(pady=5)
        self.driver_window.bind('<Return>', self.lap)
        # Stop/Plot Button
        self.btn_stop = Button(self.driver_window, width=20, text="Stop [ESC]", command=self.stop_and_plot)
        self.btn_stop.pack(pady=5)
        self.driver_window.bind("<Escape>", self.stop_and_plot)
        # Checkbox
        self.checkVar = IntVar(value=0)
        self.chk = Checkbutton(self.driver_window, text="Plot Laptimes when Stop", variable=self.checkVar)
        self.chk.pack(pady=2)
        # Save Button
        self.btn_save_summary = ttk.Button(self.driver_window, text = 'Save Summary [F1]', command = self.save_summary)
        self.btn_save_summary.place(x=0, y=0, width = 130)
        self.btn_save_csv = ttk.Button(self.driver_window, text = 'Save CSV [F2]', command = self.save_csv)
        self.btn_save_csv.place(x=0, y=30, width = 130)

        # Label for last Lap Time
        self.times = Label(self.driver_window)
        self.times.config(background="white", font = ('calibri', 15, 'bold'))
        self.times.pack(anchor = 'center')

        # TextField for all Lap Times
        self.scroll_bar = Scrollbar(self.driver_window)
        self.times_text = Text(self.driver_window, height=10, width=80)
        self.scroll_bar.pack(side=RIGHT, fill=Y)
        self.times_text.pack(side=LEFT, fill=Y)
        self.scroll_bar.config(command=self.times_text.yview)
        self.times_text.config(yscrollcommand=self.scroll_bar.set)
        self.initial = "Laptimes Log:"
        self.times_text.insert(END, self.initial)

        ###### GLOBAL VARIABLES #####
        self.start_time = 0
        self.start_times = []
        self.lap_times = [] # save clock-time when new lap begins in list
        self.lap_times_delta = {} # save lap-times in dict
        self.doTimer = True
        self.times_text_object_variable = ""
        self.caching = caching

        # Catch OnClose Event
        self.driver_window.protocol("WM_DELETE_WINDOW", self.on_closing)

        # Short Cuts Saving
        self.driver_window.bind("<F1>", self.save_summary)
        self.driver_window.bind("<F2>", self.save_csv)

    def save_summary(self, event=None):
        save_me.save_summary(self.start_times, self.lap_times_delta, self.times_text.get('1.0','end'))

    def save_csv(self, event=None):
        save_me.save_csv(self.lap_times_delta)
    
    def time(self): 
        """Time & Stopwatch"""
        # Check, if Timer should be stopped
        if not self.doTimer:
            self.lbl.config(text = "00:00:00")
            self.lbl_delta.config(text = "00:00:00.0000")
            return
        now = datetime.now()
        string_delta = now.strftime('%H:%M:%S.%f')
        string_clock = now.strftime('%H:%M:%S')
        delta = helper.calc_delta(self.start_time, string_delta)
        self.lbl.config(text = string_clock)
        self.lbl_delta.config(text = str(delta)[:-2])
        self.lbl.after(10, self.time)

    def lap(self, event=None):
        clock_stand = self.lbl.cget("text")
        stopwatch_stand = self.lbl_delta.cget("text")
        new = "\nLap " + str(len(self.lap_times)+1) + ": " + str(stopwatch_stand)
        self.lap_times_delta[len(self.lap_times)+1] = str(stopwatch_stand)
        self.lap_times.append(clock_stand)
        self.set_start_time()
        self.times.config(text = new)
        self.times_text.insert(END, new + " Comment: ")

    def set_start_time(self):
        self.start_time = datetime.now().strftime('%H:%M:%S.%f')
        self.start_times.append(self.start_time)

    def startBtn_OnClick(self, event=None):
        self.doTimer = True
        self.set_start_time()
        self.times_text.insert(END, "\nSession (re)started: " + self.start_time[:-3])
        self.time()

    def stop_and_plot(self, event=None):
        self.doTimer = False
        self.times_text_object_variable = self.times_text.get('1.0','end')
        if len(self.lap_times_delta) > 0:
            if self.checkVar.get() == 1:
                plot_me.plotting(self.driver_window, self.lap_times_delta, self.driver_name, self.times_text_object_variable, self.start_times)
            if self.caching:
                save_me.quick_caching(self.start_times, self.lap_times_delta, self.times_text_object_variable)

    def on_closing(self):
        self.times_text_object_variable = self.times_text.get('1.0','end')
        self.driver_window.destroy()
