# Racing Stopwatch

## Get Started - For Developers
### Step 1: Local Configurations
Add a local file 'local_config.py'
You need the following variables
- logo_name = "logo.png" (file_name of the logo)
- logo_path = "/Users/xxxxxx/logo.png" (full path to the logo)
- path_to_logo = "/Users/xxx/stopwatch_files/" (this is the path to the directory the logo is in)
- caching_path = path_to_logo (this will be the caching path to save infos during runtime)
- team_name = "Racing Team Name"

### Step 2: Compile
Compile everything using 'pyinstaller' (see command_line.txt)
- using --onedir will be quicker in the app start-up than --onefile

## Get Started - For Users
### Step 1: Copy
Copy the full directory, .exe (windows) or .app (mac) onto you computer

### Step 2: Create Directory
Create the directory/ies for the caching and/or logo specified and put the logo into the directory
- Note: This is not mandatory as the stopwatch will also work without this step, but in order to see the individual logo and to use the caching functionality, it is recommended to create this directory

### Step 3: Execute the App


