from datetime import datetime, timedelta
from time import strftime
import statistics

def calc_delta(prev, now):
    """ Calculate Delta of two time strings (prev, now)"""
    p = datetime.strptime(prev, '%H:%M:%S.%f')
    n = datetime.strptime(now, '%H:%M:%S.%f')
    return n-p

def session_time_total(lap_times_delta):
    summe = 0
    for delta_time in lap_times_delta:
        p = datetime.strptime(lap_times_delta[delta_time], '%H:%M:%S.%f')
        delta = timedelta(hours=p.hour, minutes=p.minute, seconds=p.second, microseconds=p.microsecond)
        if summe == 0:
            summe = delta
        else:
            summe = summe + delta
    return summe

def min_to_sec(timestring):
    pt = datetime.strptime(timestring,'%H:%M:%S.%f')
    total_seconds = pt.hour*60*60 + pt.second + pt.minute*60 + pt.microsecond / 1000000
    return total_seconds

def get_median(times):
    return statistics.median(times)

def get_mean(times):
    return statistics.mean(times)

def get_sd(times):
    return statistics.pstdev(times)

