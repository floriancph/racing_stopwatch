import csv
from tkinter.filedialog import asksaveasfile
import random
import datetime

import helper, local_config

def save_summary(start_times, lap_times_delta, times_text):
    
    files = [('Text Document', '*.txt')] 
    file = asksaveasfile(filetypes = files, defaultextension = ".txt")
    
    try:
        file = open(file.name, "w")
        file.write(f"Session Start-Time: {str(start_times[0])}\n")
        file.write(f"Session Duration: {str(helper.session_time_total(lap_times_delta))}\n")
        file.write(f"# Laps: {len(lap_times_delta)}\n")
        file.write(f"Lap-Times:\n{times_text}\n")
        file.close()
    except Exception as e:
        print(e)

def save_csv(lap_times_delta): 

    files = [('CSV', '*.csv')] 
    file = asksaveasfile(filetypes = files, defaultextension = ".csv")
    
    with open(file.name, mode='w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(['Lap', 'Time', 'Time_in_seconds'])
        for t in lap_times_delta:
            csv_writer.writerow([t, lap_times_delta[t], helper.min_to_sec(lap_times_delta[t])])


def quick_caching(start_times, lap_times_delta, times_text):
    try:
        now = datetime.datetime.now()
        filename = str(now.year) + str(now.month) + str(now.day) + str(now.hour) + str(now.second) + str(now.microsecond) + "_caching.txt"
        file_path = local_config.caching_path + filename
        file = open(file_path, "w")
        file.write(f"Caching-Time: {str(now)}\n")
        file.write(f"Session Start-Time: {str(start_times[0])}\n")
        file.write(f"Session Duration: {str(helper.session_time_total(lap_times_delta))}\n")
        file.write(f"# Laps: {len(lap_times_delta)}\n")
        file.write(f"Lap-Times:\n{times_text}\n")
        file.close()
    except Exception as e:
        print(e)